#! /usr/bin/env python2

from __future__ import division
import sys,os
sys.path = ["../source"] + sys.path
from setup import *

# values for limit setting
n=1
nBkg     = 9.60053680468*n
nObs     = 6.0*n
npWidth1 = 0.458967154816
npWidth2 = 2.10999625248
npWidth3 = 0.180570557745

# set limits
# These limit functions are in source/limits.py
# 1) Set limits with nSig as POI
limit1 = limits.nSigLimit(nBkg,nObs,npWidth1=npWidth1,npWidth2=npWidth2,npWidth3=npWidth3)
# 2) Set limits with f(lambda)=nSig, lambda is POI
limit2 = limits.lambdaLimit(nBkg,nObs,npWidth1=npWidth1,npWidth2=npWidth2,npWidth3=npWidth3)

# output
print "="*50
print "Limit set with nSig as POI:"
print "nSig <",limit1
print "-"*50
print "Limit set with Lambda as POI:"
print "nSig <",limit2
print "="*50
