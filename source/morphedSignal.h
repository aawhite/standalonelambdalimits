#ifndef HCLASS
#define HCLASS

#include "RooAbsReal.h"
#include "RooRealProxy.h"
#include "RooTrace.h"

class RooRealVar;

/*
################################################################################
#                                                                              #
#                             HMorph and HMorphNorm                            #
#                                                                              #
#  There are two classes in this file:                                         #
#  1) HMorph: a PDF to morph between an arbitrary number of input histogram    #
#             the class does interpolations both between the bins of the       #
#             histograms (mll dimension) and the histograms (labda dimension)  #
#  2) HMorphNorm: a RooRealVar to morph between the normalizations provided    #
#                 by the input histograms. It is associated to HMorph, and     #
#                 currently is hard coded to request an integral in "fitRange" #
#  These classes work together to provide a model in an extended pdf           #
#                                                                              #
################################################################################
*/

#define classMode RooAbsReal
// #define classMode RooAbsPdf

#define normalized true
// #define normalized false

class HMorph : public classMode {
public:

    HMorph(const char *name, const char *title,
           RooAbsReal& _mll, RooAbsReal& _lambda) :
        _initialized(0),
        classMode(name,title),
        mll("mll","Observable",this,_mll),
        lambda("lambda","lambda",this,_lambda)
    {};

    HMorph(const HMorph& other, const char* name=0) :
        classMode(other,name),
        mll("mll",this,other.mll),
        lambda("lambda",this,other.lambda)
    {
       _nBins = other._nBins;
       _minBin = other._minBin;
       _maxBin = other._maxBin;
       _binWidth = other._binWidth;
       _initialized = other._initialized;
       int nHists = other._lambdas.size();
       TH1* newHist;
       for (int i=0; i<nHists; i++){
           // newHist = (TH1*)other._hists[i]->Clone();
           newHist = other._hists[i];
           _hists.push_back(newHist);
           _lambdas.push_back(other._lambdas[i]);
           _norms.push_back(other._norms[i]);
       }
    };

    inline virtual ~HMorph() {
        // cout<<">>>>>>>>>>>>>>>>>>>>>>>>\n";
        // cout<<"Calling HMorph Destructo\n";
        // cout<<"<<<<<<<<<<<<<<<<<<<<<<<<\n";
        return;
    }

    virtual TObject* clone(const char* newname) const { return new HMorph(*this,newname); }

    // #####################################
    // variables
    // #####################################
    int _nBins=-1;           // number of bins, should be the same for all histograms
    double _minBin=-1;       // min bin, should be the same for all histograms
    double _maxBin=-1;       // max bin, should be the same for all histograms
    double _binWidth=-1;     // bin width, should be the same for all histograms, all bins
    bool _initialized;       // keeps track of whether the class has been initialized
    vector<TH1*> _hists;     // vector of histograms
    vector<double> _lambdas; // morphing parameter value
    vector<double> _norms;   // histogram normalizaitons
    RooRealProxy mll ;       // x/binn dimension variable
    RooRealProxy lambda ;    // morphing dimension variable

    void add(TH1* hist, double paramValue){
        // Add a histogram associated to paramValue. This is a public interface func
        // If not _initialized, define binning
        // If _initialized, check against binning
        bool status;
        if (!_initialized){
            status = this->_addFirst(hist,paramValue);
        }
        else{
            status = this->_addHist(hist,paramValue);
        }
        if (!status){
            cout<<"\n\n==== ERROR ADDING HIST\n";
            cout<<"==== Hist:\n";
            hist->Print();
            cout<<"\n";
        }
    }

    bool _addFirst(TH1* hist, double paramValue){
        // add the first histogram
        _initialized = true;
        // set bin params
        _nBins    = hist->GetNbinsX();
        _minBin   = hist->GetBinLowEdge(1);
        _maxBin   = hist->GetBinLowEdge(_nBins+1);
        _binWidth = hist->GetBinWidth(1);
        cout<<"Added first histogram:"<<endl;
        cout<<"GetNbinsX:"<<hist->GetNbinsX()<<endl;
        cout<<"GetBinLowEdge:"<<hist->GetBinLowEdge(1)<<endl;
        cout<<"GetBinLowEdge:"<<hist->GetBinLowEdge(_nBins+1)<<endl;
        cout<<"GetBinWidth:"<<hist->GetBinWidth(1)<<endl;
        // add hist
        return this->_addHist(hist,paramValue);
    }

    Double_t analyticalIntegral(Int_t code, const char* rangeName) const{
        // Implemented to avoid a numeric integration of pdf
        if (normalized){

            // cout<<"\n======== INTEGRAL ========\n";
            // cout<<"Lambda: "<<lambda<<endl;
            // cout<<"rangeName: "<<rangeName<<endl;
            // cout<<this->trueIntegral(rangeName)<<endl;
            // cout<<this->trueIntegral("sr")<<endl;
            // cout<<this->trueIntegral("fitRange")<<endl;
            // cout<<"==========================\n";

            return this->trueIntegral(rangeName)/this->trueIntegral("fitRange");
            // return 1;
        }
        else
            return this->trueIntegral(rangeName);
    }

    Int_t getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* rangeName) const{
        // Implemented to avoid a numeric integration of pdf
        // copy variable which can integrate over to analVars
        analVars.add(*(allVars.find("x")));
        return 1;
    }

    Double_t trueIntegral(const char* rangeName) const {
        // get range of integral: if rangeName is provided, use this
        if (!_initialized){
            cout<<"## Warning: HMorph not init, returning 0 ##\n";
            return 0;
        }
        double rMin = mll.min(rangeName);
        double rMax = mll.max(rangeName);
        int morphIndex = this->_getMorphIndex();
        int minBin = _hists[0]->FindBin(rMin);
        int maxBin = _hists[0]->FindBin(rMax);
        double x1 = _lambdas[morphIndex];
        double x2 = _lambdas[morphIndex+1];
        double x  = lambda;
        // integrate hists
        double y1 = _hists[morphIndex]->Integral(minBin,maxBin);
        double y2 = _hists[morphIndex+1]->Integral(minBin,maxBin);
        // interpolate

        // cout<<"TEST\n";
        // // cout<<"val="<<_hists[morphIndex]->GetBinContent(1000)<<endl;
        // // cout<<"val="<<_hists[morphIndex]->Integral(500,2000)<<endl;
        // cout<<"INTEGRATE RANGE: NAME="<<rangeName<<endl;
        // cout<<"INTEGRATE RANGE: GeV="<<rMin<<endl;
        // cout<<"INTEGRATE RANGE: GeV="<<rMax<<endl;
        // cout<<"INTEGRATE RANGE: Bin="<<minBin<<endl;
        // cout<<"INTEGRATE RANGE: Bin="<<maxBin<<endl;
        // cout<<"INTEGRATE RANGE: "<<y1<<endl;
        // cout<<"INTEGRATE RANGE: "<<y2<<endl;

        // _hists[morphIndex]->Print();
        // _hists[morphIndex+1]->Print();
        double integral = this->_interpolate(x,x1,y1,x2,y2);
        // cout<<"DONE\n";
        return integral;
    };

    // void debug(){
    //     TH1* h = _hists[0];
    //     cout<<"=========== DEBUG =============\n";
    //     for (int i=0; i<h->GetNbinsX(); i++){
    //         cout<<h->GetBinContent(i)<<endl;
    //     }
    // }

    // void debug(){
    //     TH1* h;
    //     cout<<"=========== DEBUG =============\n";
    //     for (int i=0; i<_hists.size(); i++){
    //         h = _hists[i];
    //         h->Print();
    //     }
    //     cout<<"WIDTH="<<h->GetBinWidth(2)<<endl;
    //     cout<<"center="<<h->GetBinCenter(100)<<endl;
    //     cout<<"trueIntegral:"<<this->trueIntegral("fitRange")<<endl;
    // }

    double debug(){
        cout<<"\n=========== DEBUG =============\n";
        cout<<"lambda: "<<lambda<<endl;
        cout<<"morph index: "<<this->_getMorphIndex()<<endl;
        cout<<"===============================\n";
        return this->evaluate();
    }


protected:

    bool _addHist(TH1* hist, double paramValue){
        // add a histogram and param
        // check bins for compatibility
        if ( hist->GetNbinsX() != _nBins              ||
             hist->GetBinLowEdge(1) != _minBin        ||
             hist->GetBinLowEdge(_nBins+1) != _maxBin ||
             hist->GetBinWidth(1) != _binWidth
           ) { 
               cout<<"==================== DEBUG CRASH ====================\n";
               cout<<"GetNbinsX:"<<hist->GetNbinsX()<<"=="<<_nBins<<endl;
               cout<<"GetBinLowEdge:"<<hist->GetBinLowEdge(1)<<"=="<<_minBin<<endl;
               cout<<"GetBinLowEdge:"<<hist->GetBinLowEdge(_nBins+1)<<"=="<<_maxBin<<endl;
               cout<<"GetBinWidth:"<<hist->GetBinWidth(1)<<"=="<<_binWidth<<endl;
               cout<<"=====================================================\n";
               throw std::runtime_error("Issue with added histogram"); 
           }
        // add histogram
        TH1* copyHist;//needed?
        copyHist = (TH1*)hist->Clone();
        _hists.push_back(copyHist);
        _lambdas.push_back(paramValue);
        _norms.push_back(this->_th1Sum(copyHist));
        return true;
    }

    double _th1Sum(TH1* hist){
        // return integral of th1
        double result = 0;
        for (int i=1; i<hist->GetNbinsX(); i++)
            result+=hist->GetBinContent(i);
        return result;
    }


    double _interpolate(double x, double x1, double y1, double x2, double y2) const {
        // perform interpolation
        // y1 = y1>0?y1:0;
        // y2 = y2>0?y2:0;
        if (x2-x1==0) throw std::runtime_error("Division by zero in Morphed interpolation, x2-x1");
        double m  = (y2-y1)/(x2-x1);
        double y  = y1 + m*(x-x1);
        return y;
    }

    double _getInterpolatedHistValue(TH1* hist) const{
        // return a value interpolated between the bins of the histogram
        // the returned values should be continuous (a requirement of RooAddPdf)
        if (!hist)
           throw std::runtime_error("Expected histogram not found!"); 
        double x = mll;
        double binIndex = hist->FindBin(x);
        double binCenter = hist->GetBinCenter(binIndex);
        bool isUpper     = (x>binCenter);
        double ret;
        // isUpper=true;
        // isUpper=false;
        // cout<<"isUpper: "<<isUpper<<" x="<<x<<" binCenter="<<binCenter<<" -1="<<hist->GetBinCenter(binIndex-1)<<" +1="<<hist->GetBinCenter(binIndex+1)<<endl;
        // cout<<"evaluate:: "<<binIndex<<" "<<hist->GetNbinsX()<<endl;
        // cout<<"evaluate:: "<<_binWidth<<endl;

        if (binIndex==hist->GetNbinsX()+1) return 0;
        if (binIndex==0) return 0;

        if (isUpper){
            double x1 = hist->GetBinCenter(binIndex);
            double x2 = hist->GetBinCenter(binIndex+1);
            double y1 = hist->GetBinContent(binIndex);
            double y2 = hist->GetBinContent(binIndex+1);
            ret = this->_interpolate(x,x1,y1,x2,y2)/_binWidth;
            // ret = this->_interpolate(x,x1,y1,x2,y2);
        }
        else{
            double x1 = hist->GetBinCenter(binIndex-1);
            double x2 = hist->GetBinCenter(binIndex);
            double y1 = hist->GetBinContent(binIndex-1);
            double y2 = hist->GetBinContent(binIndex);
            ret = this->_interpolate(x,x1,y1,x2,y2)/_binWidth;
            // cout<<"\t\t"<<y1<<" "<<y2<<" "<<ret<<endl;
            // ret = this->_interpolate(x,x1,y1,x2,y2);
        }
        return ret;
    }

    int _getMorphIndex() const{
        // return index of the first _lambdas value which is below lambda->getVal()
        // if lowest _lambdas is higher, returns 0
        // if highest _lambdas is lower, returns nHists-1 (2nd last lambda index)
        double target = lambda;
        double nHists = _hists.size();
        // cout<<"getMorphIndex:: "<<target<<endl;
        // return 20;
        // edge cases
        if (_lambdas[0]>=target) return 0;
        if (_lambdas[nHists-1]<=target) return nHists-2;
        // search
        for (int i=1; i<nHists; i++){
            // cout<<"check: "<<_lambdas[i]<<" "<<target<<endl;
            if (_lambdas[i]>target) return i-1;
        }
        // should not happen
        cout<<"\n\n";
        cout<<"Finding index for target: "<<target<<endl;
        cout<<"Number of hists:          "<<nHists<<endl;
        cout<<"Printing lambda:          "<<endl;
        lambda.Print();
        cout<<"Has value:                "<<lambda<<endl;
        cout<<"\n\n";
        // lambda.setVal(10);
        throw std::runtime_error("Could not find HMorph index in getMorphIndex");
        return 0;
    }




    Double_t evaluate() const{
        // return 1; //todo
        // if (!_initialized) return 1;
        // evaluate interpolated value at bin corresponding to mass variable mll
        // 1) find histogram index corresponding to morphing variable
        if (!_initialized) return 0;
        double morphIndex = this->_getMorphIndex();
        if (!_hists[morphIndex] || !_hists[morphIndex+1])
           throw std::runtime_error("Expected histogram not found!"); 
        double binIndex = _hists[morphIndex]->FindBin(mll);
        // ###################################
        // double morphIndex = 0;
        // double binIndex = 15;
        // 3) get interpolation between bins of histograms (mll direction)

       // // cout<<mll<<" "<<binIndex<<endl;
        // // _hists[morphIndex]->Print();
        // double x = mll;
        // TH1* hist = _hists[morphIndex];
        // double binCenter = hist->GetBinCenter(binIndex);
        // bool isUpper     = (x>binCenter);
        // double ret;
        // double x1 = hist->GetBinCenter(binIndex);
        // double x2 = hist->GetBinCenter(binIndex+1);
        // double y1 = hist->GetBinContent(binIndex);
        // double y2 = hist->GetBinContent(binIndex+1);
        // // hist->Print();
        // // cout<<_lambdas[morphIndex]<<endl;
        // // cout<<binIndex<<endl;
        // // hist->Print();
        // // return y1/trueIntegral("fitRange");
        // // return y1/trueIntegral("fitRange")/_binWidth;
        // // return y1/trueIntegral("fitRange")/_binWidth/_binWidth;
        // // return y1/trueIntegral("fitRange")/_binWidth/_binWidth/_binWidth;
        // // return y1/trueIntegral("fitRange");
        // // return y1/trueIntegral("fitRange")/_binWidth;
        // ret = this->_interpolate(x,x1,y1,x2,y2)/_binWidth/trueIntegral("fitRange");
        // return ret;
        // // return y1/this->trueIntegral("fitRange");

        double y1 = this->_getInterpolatedHistValue(_hists[morphIndex]);
        double y2 = this->_getInterpolatedHistValue(_hists[morphIndex+1]);
        // 4) get values of lambda
        double x1 = _lambdas[morphIndex];
        double x2 = _lambdas[morphIndex+1];
        double x  = lambda;
        // 5) interpolate over lambda direction
        double y = this->_interpolate(x,x1,y1,x2,y2);
        // cout<<"DEBUG:                    "<<lambda<<" "<<mll<<" "<<y<<endl;
        // y = y1;

        if (normalized){
            y/=abs(this->trueIntegral("fitRange"));
            // y/=this->trueIntegral("fitRange");
            // y/=10000;
        }

        // return -y;
        // if (y<0) return 0;
        // return abs(y);
        return y;

    }

private:
    ClassDef(HMorph,1) // Gaussian PDF
};


class HMorphNorm : public RooAbsReal {
public:

    HMorphNorm(const char *name, const char *title, HMorph& morphInput,
               RooAbsReal& _mll, RooAbsReal& _lambda) :
        RooAbsReal(name,title),
        _morphInternal("morph","morph",this,morphInput),
        lambda("lambda","lambda",this,_lambda)
    {
    };

    HMorphNorm(const HMorphNorm& other, const char* name=0) :
        RooAbsReal(other,name),
        _morphInternal("morph",this,other._morphInternal),
        lambda("lambda",this,other.lambda)
    {
    };

    virtual TObject* clone(const char* newname) const { return new HMorphNorm(*this,newname); }
    inline virtual ~HMorphNorm() {  }

    RooRealProxy _morphInternal ;
    RooRealProxy lambda ;
protected:
    Double_t evaluate() const {
        // return the integral in the fit range for the HMorph
        // return 0.00000001;
        // return lambda;
        // return lambda/2.0;

        if (lambda>100) {
            // throw std::runtime_error("Lambda larger than 100"); 
            return 0;
        }
        // return 1;
        // return abs(((HMorph*)_morphInternal.absArg())->trueIntegral("fitRange"))/10;
        return abs(((HMorph*)_morphInternal.absArg())->trueIntegral("fitRange")); //good
        // return ((HMorph*)_morphInternal.absArg())->trueIntegral("fitRange");
    };
private:
    ClassDef(HMorphNorm,1) // Gaussian PDF
};


/* This calss is a simplified version of HMorph and HMorphNorm
   It takes an integral of the appropriate CI lambda histogram
*/
class HInt : public RooAbsReal {
public:

    HInt(const char *name, const char *title,
         RooAbsReal& _lambda,
         bool invert,
         double srMin,double srMax) :
        _invert(invert),
        _srMin(srMin),
        _srMax(srMax),
        RooAbsReal(name,title),
        lambdaInv("lambdaInv","lambdaInv",this,_lambda)
    {};

    HInt(const HInt& other, const char* name=0) :
        RooAbsReal(other,name),
        lambdaInv("lambdaInv",this,other.lambdaInv)
    {
       int nHists = other._lambdas.size();
       TH1* newHist;
       _invert = other._invert;
       _srMin = other._srMin;
       _srMax = other._srMax;
       _loBin = other._loBin;
       _hiBin = other._hiBin;
       for (int i=0; i<nHists; i++){
           newHist = other._hists[i];
           _hists.push_back(newHist);
           _lambdas.push_back(other._lambdas[i]);
       }
    };

    inline virtual ~HInt() {
        // cout<<">>>>>>>>>>>>>>>>>>>>>>>>\n";
        // cout<<"Calling HInt Destructo\n";
        // cout<<"<<<<<<<<<<<<<<<<<<<<<<<<\n";
        return;
    }

    virtual TObject* clone(const char* newname) const { return new HInt(*this,newname); }

    // #####################################
    // variables
    // #####################################
    bool _initialized;       // keeps track of whether the class has been initialized
    vector<TH1*> _hists;     // vector of histograms
    vector<double> _lambdas; // morphing parameter value
    RooRealProxy lambdaInv ;    // morphing dimension variable
    double _srMin;
    bool _invert;
    double _srMax;
    int _loBin;
    int _hiBin;

    void add(TH1* hist, double paramValue){
        // Add a histogram associated to paramValue.
        // This gets added, coppied, and stored for integrating
        bool status;
        status = this->_addHist(hist,paramValue);
        if (!status){
            cout<<"\n\n==== ERROR ADDING HIST\n";
            cout<<"==== Hist:\n";
            hist->Print();
            cout<<"\n";
        }
        _loBin = hist->FindBin(_srMin);
        _hiBin = hist->FindBin(_srMax);
    }

    double debug(){
        cout<<"\n=========== DEBUG =============\n";
        cout<<"lambdaInv: "<<lambdaInv<<endl;
        cout<<"===============================\n";
        return this->evaluate();
    }


protected:

    bool _addHist(TH1* hist, double paramValue){
        // add a histogram and param
        // check bins for compatibility
        // add histogram
        TH1* copyHist;//needed?
        copyHist = (TH1*)hist->Clone();
        _hists.push_back(copyHist);
        _lambdas.push_back(paramValue);
        return true;
    }

    double _th1Sum(TH1* hist){
        // return integral of th1
        double result = 0;
        for (int i=1; i<hist->GetNbinsX(); i++)
            result+=hist->GetBinContent(i);
        return result;
    }

    Double_t _getLambda() const{
        // do lambda inversion in one place
        // or don't do it
        if (_invert){
            // return (100+lambdaInv);
            return (-lambdaInv);
        }
        else{
            return (lambdaInv);
        }
    }

    Double_t evaluate() const{
        // cout<<":"<<lambdaInv<<":"<<endl;
        int morphIndex = this->_getMorphIndex();
        double lambdaVal = this->_getLambda();
        if (!_hists[morphIndex] || !_hists[morphIndex+1])
           throw std::runtime_error("Expected histogram not found!"); 

        TH1* h1 = _hists[morphIndex];
        TH1* h2 = _hists[morphIndex+1];

        // cout<<"\n==========================\n";
        // cout<<"== eval\n";
        // cout<<"lambdaVal="<<lambdaVal<<endl;
        // cout<<"morphIndex="<<morphIndex<<endl;
        // cout<<"_loBin="<<_loBin<<endl;
        // cout<<"_hiBin="<<_hiBin<<endl;
        // cout<<"==========================\n";

        double h1Int = h1->Integral(_loBin,_hiBin);
        double h2Int = h2->Integral(_loBin,_hiBin);

        double lambda1 = _lambdas[morphIndex];
        double lambda2 = _lambdas[morphIndex+1];

        double ret = this->_interpolate(lambdaVal,lambda1,h1Int,lambda2,h2Int);
        return ret;
        // return ret/exp(pow(ret,0.87)/11);
    }

    double _interpolate(double x, double x1, double y1, double x2, double y2) const {
        // perform interpolation
        if (x2-x1==0) throw std::runtime_error("Division by zero in Morphed interpolation, x2-x1");
        double m  = (y2-y1)/(x2-x1);
        double y  = y1 + m*(x-x1);
        return y;
    }

    int _getMorphIndex() const{
        // return index of the first _lambdas value which is below lambdaInv->getVal()
        // if lowest _lambdas is higher, returns 0
        // if highest _lambdas is lower, returns nHists-1 (2nd last lambdaInv index)
        double target = this->_getLambda();
        double nHists = _hists.size();
        // edge cases
        if (_lambdas[0]>=target) return 0;
        if (_lambdas[nHists-1]<=target) return nHists-2;
        // search
        for (int i=1; i<nHists; i++){
            // cout<<"check: "<<_lambdas[i]<<" "<<target<<endl;
            if (_lambdas[i]>target) return i-1;
        }
        // should not happen
        cout<<"\n\n";
        cout<<"Finding index for target: "<<target<<endl;
        cout<<"Number of hists:          "<<nHists<<endl;
        cout<<"Printing lambda:          "<<endl;
        lambdaInv.Print();
        cout<<"Has value:                "<<lambdaInv<<endl;
        cout<<"\n\n";
        throw std::runtime_error("Could not find HMorph index in getMorphIndex");
        return 0;
    }

};

ClassImp(HMorph);
ClassImp(HMorphNorm);
ClassImp(HInt);

#endif
