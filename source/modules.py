from __future__ import division

################################################################################
# Import the messy root libraries
################################################################################

print "rootLibs"

import sys,os
sys.path = [os.path.realpath(__file__)] + sys.path
sys.path = ["/home/prime/Downloads/buildRoot/lib/"] + sys.path
print "Loading root"

import ROOT
from ROOT.RooStats import *
from ROOT.RooFit import *
from ROOT import RooAbsPdf
from ROOT import RooWorkspace
from ROOT import RooRealVar
from ROOT import RooDataHist
from ROOT import RooArgList
from ROOT import RooArgSet
from ROOT import RooBinning
from ROOT import RooLinearVar
from ROOT import RooAbsReal
from ROOT import RooRealSumPdf
from ROOT import RooExtendPdf
from ROOT import RooAddPdf
from ROOT import RooRealSumPdf
from ROOT import RooHistPdf
from ROOT import RooGaussian
from ROOT import RooFitResult
from ROOT import RooRandom
from ROOT import RooDataSet

# for limits
from ROOT.RooStats import ModelConfig
from ROOT.RooStats import AsymptoticCalculator
from ROOT.RooStats import HypoTestInverter
from ROOT.RooStats import HypoTestCalculatorGeneric


from ROOT import TRandom
from ROOT import TFile
from ROOT import TH1D
from ROOT import TH1F
from ROOT import TCanvas
from ROOT import TImage

from ROOT import kBlack
from ROOT import kRed

from ROOT.RooFit import RecycleConflictNodes
from ROOT.RooFit import DataError

import numpy as np

from collections import defaultdict

RooFit = ROOT.RooFit

import modelConstructor as mc
import copy


def yellow(*string):
    """ return string as red """
    string = [str(s) for s in string]
    ret = "\033[33m{0}\033[39m".format(" ".join(string))
    return ret

def red(*string):
    """ return string as red """
    string = [str(s) for s in string]
    ret = "\033[31m{0}\033[39m".format(" ".join(string))
    return ret

def green(*string):
    """ return string as green """
    string = [str(s) for s in string]
    ret = "\033[32m{0}\033[39m".format(" ".join(string))
    return ret

def blue(*string):
    """ return string as green """
    string = [str(s) for s in string]
    ret = "\033[34m{0}\033[39m".format(" ".join(string))
    return ret



