from __future__ import division
from modules import *
from modules import mc as modelConstructor

# file with limit setting functions 

def nSigLimit(nBkg, nObs,i=1,interference="const",chirality="LL",channel="ee",npWidth1=1,npWidth2=1,npWidth3=1):
    """ calculate single bin limit on nSig
        np1 = nuissance parameter width  number:1
        nBkg is background expectation
        nObs is events observed
    """

    ##################################################
    # make workspace
    ##################################################
    w = RooWorkspace("w","w")
    w.factory("nObs[{0}]".format(nObs))
    w.factory("nBkg[{0}]".format(nBkg))
    w.factory("x[130,0,6000]")
    w.var("x").setConstant()
    x = w.var("x")

    # this function builds the RooAbsReal wrapper
    # I put this in the nSig limit to keep it the same 
    # as the lambda limit procedure
    w.factory("lambda[0,100]")
    poiName = "lambda"
    nSig = modelConstructor.nSigBuilder(w)
    getattr(w,"import")(nSig)


    ##################################################
    # make poisson model
    ##################################################
    # w.factory("sum:nExp(nSig,prod:bkgProd(sum:bkgSum(1,np1[0,-1,1],np2[0,-1,1],np3[0,-1,1]),nBkg))")
    w.factory("sum:nExp(nSig,prod:bkgProd(sum:bkgSum(1,np1[0,-5,5],np2[0,-5,5],np3[0,-5,5]),nBkg))")
    w.factory("Gaussian:g1(np1,0,npWidth1[{0}])".format(npWidth1/nBkg));
    w.factory("Gaussian:g2(np2,0,npWidth2[{0}])".format(npWidth2/nBkg));
    w.factory("Gaussian:g3(np3,0,npWidth3[{0}])".format(npWidth3/nBkg));
    w.factory("Poisson:pois(nObs,nExp)")
    w.factory("PROD:model(pois,g1,g2,g3)");
    # w.factory("PROD:model(pois,a[1])");
    # w.factory("PROD:model(pois,g1,g2)");

    data = RooDataSet("data","", RooArgSet(w.var("nObs")))
    w.var("nObs").setVal(nObs)
    data.add(RooArgSet(w.var("nObs")))
    getattr(w,"import")(data)

    ##################################################
    # create S+B hypothesis H1
    ##################################################
    sbModel = ModelConfig("sbModel",w)
    sbModel.SetPdf(w.pdf("model"))
    sbModel.SetParametersOfInterest(RooArgSet(w.var(poiName)))
    sbModel.SetObservables(RooArgSet(w.var("nObs")))
    sbModel.SetNuisanceParameters(RooArgSet(w.var("np1"),w.var("np2"),w.var("np3")))
    sbModel.SetSnapshot(RooArgSet(w.var(poiName)))
    ##################################################

    ##################################################
    # create null hypothesis H0 (based on H)
    ##################################################
    bModel = sbModel.Clone()
    bModel.SetName("bModel")
    w.obj(poiName).setVal(0)
    bModel.SetSnapshot(RooArgSet(w.obj(poiName)))
    ##################################################

    ##################################################
    # create hypotest inverter
    ##################################################
    ac = AsymptoticCalculator(data, bModel, sbModel)
    ac.SetOneSided(True)
    # ac.SetOneSided(0)
    hypoCalc = HypoTestInverter(ac)
    hypoCalc.SetFixedScan(100,0.1,10,True)
    hypoCalc.SetConfidenceLevel(0.95)
    hypoCalc.UseCLs(True)
    # hypoCalc.UseCLs(0)

    htr = hypoCalc.GetHypoTestCalculator().GetHypoTest()
    htir = hypoCalc.GetInterval()
    w.obj(poiName).setVal(htir.UpperLimit())
    ret = w.obj("nSig").getVal()

    print "-"*50
    print "model",w.obj("model").getVal()
    print "nExp",w.obj("nExp").getVal()
    print "np1",w.var("np1").getVal()
    print "np2",w.var("np2").getVal()
    print "np3",w.var("np3").getVal()
    print "-"*50

    return ret

def lambdaLimit(nBkg, nObs,i=1,interference="const",chirality="LL",channel="ee",npWidth1=1,npWidth2=1,npWidth3=1):
    """ calculate single bin limit on lambda
        np1 = nuissance parameter width  number:1
        nBkg is background expectation
        nObs is events observed
    """
    ##################################################
    # make workspace
    ##################################################
    w = RooWorkspace("w","w")
    w.factory("nObs[{0}]".format(nObs))
    w.factory("nBkg[{0}]".format(nBkg))
    w.factory("x[130,0,6000]")
    w.var("x").setConstant()
    w.var("nBkg").setConstant()
    x = w.var("x")

    ##################################################
    # make lambda
    ##################################################
    # inverted lambda, used for limits
    w.factory("lambdaInv[-100,-12]")

    poiName = "lambdaInv"
    rebinScale   = 1
    lumi         = 139
    srMin = 2000
    srMax = 6000
    # Set up morphed signal
    hint = modelConstructor.morphedIntegral(interference,chirality,
                            rebin=rebinScale,w=w,channel=channel,lumi=lumi,
                            srMin=srMin,srMax=srMax,
                           )
    hint.SetName("nSig")
    hint.SetTitle("nSig")
    getattr(w,"import")(hint)


    ##################################################
    # make poisson model
    ##################################################
    # w.factory("sum:nExp(nSig,prod:bkgProd(sum:bkgSum(1,np1[0,-1,1],np2[0,-1,1],np3[0,-1,1]),nBkg))")
    w.factory("sum:nExp(nSig,prod:bkgProd(sum:bkgSum(1,np1[0,-5,5],np2[0,-5,5],np3[0,-5,5]),nBkg))")
    w.factory("Gaussian:g1(np1,0,npWidth1[{0}])".format(npWidth1/nBkg));
    w.factory("Gaussian:g2(np2,0,npWidth2[{0}])".format(npWidth2/nBkg));
    w.factory("Gaussian:g3(np3,0,npWidth3[{0}])".format(npWidth3/nBkg));
    w.factory("Poisson:pois(nObs,nExp)")
    w.factory("PROD:model(pois,g1,g2,g3)");
    # w.factory("PROD:model(pois,a[1])");
    # w.factory("PROD:model(pois,g1,g2)");

    data = RooDataSet("data","", RooArgSet(w.var("nObs")))
    w.var("nObs").setVal(nObs)
    data.add(RooArgSet(w.var("nObs")))
    getattr(w,"import")(data)



    ##################################################
    # create S+B hypothesis H1
    ##################################################
    sbModel = ModelConfig("sbModel",w)
    sbModel.SetPdf(w.pdf("model"))
    sbModel.SetParametersOfInterest(RooArgSet(w.var(poiName)))
    sbModel.SetObservables(RooArgSet(w.var("nObs")))
    sbModel.SetNuisanceParameters(RooArgSet(w.var("np1"),w.var("np2"),w.var("np3")))
    sbModel.SetSnapshot(RooArgSet(w.var(poiName)))
    ##################################################

    ##################################################
    # create null hypothesis H0 (based on H)
    ##################################################
    bModel = sbModel.Clone()
    bModel.SetName("bModel")
    w.obj(poiName).setVal(-100)
    bModel.SetSnapshot(RooArgSet(w.obj(poiName)))
    ##################################################

    ##################################################
    # create hypotest inverter
    ##################################################
    ac = AsymptoticCalculator(data, bModel, sbModel)
    ac.SetOneSided(True)
    hypoCalc = HypoTestInverter(ac)
    hypoCalc.SetFixedScan(100,-100,-10,False)
    hypoCalc.SetConfidenceLevel(0.95)
    hypoCalc.UseCLs(True)
    # hypoCalc.UseCLs(0)

    htr = hypoCalc.GetHypoTestCalculator().GetHypoTest()
    htir = hypoCalc.GetInterval()
    w.obj(poiName).setVal(htir.UpperLimit())

    # return the nSig corresponding to the limit on lambda
    ret = w.obj("nSig").getVal()
    return ret


if __name__=="__main__":
    # some settings for limits
    nBkg= 9.60053680468
    nObs = 6.0
    npWidth1= 0.458967154816
    npWidth2= 2.10999625248
    npWidth3= 0.180570557745
    limitNSig1=nSigLimit(nBkg,nObs,npWidth1=npWidth1,npWidth2=npWidth2,npWidth3=npWidth3)
    # limitNSig1=lambdaLimit(nBkg,nObs,npWidth1=npWidth1,npWidth2=npWidth2,npWidth3=npWidth3)
    print "="*50
    print "limitNSig1",limitNSig1
    # print "limitNSig2",limitNSig2
    print "="*50

