
################################################################################
# extra.py
# Quality of life functions common to several applications
# Common imported classes
################################################################################

import sys,os, math, copy, glob
sys.path = [os.path.realpath(__file__)] + sys.path

# included to work on UMT3int01:
sys.path = ["/usr/lib64/python2.6/site-packages"] + sys.path
# included to work on UMT3int02 (not clear if needed):
sys.path = ["/usr/lib64/python2.7/site-packages"] + sys.path


import numpy as np
import numpy.ma as ma
import random
# from random import gauss
from math import sqrt


from modules import *
# RooAbsPdf = ROOT.RooFit.RooAbsPdf

# fix random seeds
random.seed(1)
np.random.seed(1)

# pystrap library
# import pystrap as ps
import limits

import cPickle as pickle

# import plotter

def yellow(*string):
    """ return string as red """
    string = [str(s) for s in string]
    ret = "\033[33m{0}\033[39m".format(" ".join(string))
    return ret

def red(*string):
    """ return string as red """
    string = [str(s) for s in string]
    ret = "\033[31m{0}\033[39m".format(" ".join(string))
    return ret

def green(*string):
    """ return string as green """
    string = [str(s) for s in string]
    ret = "\033[32m{0}\033[39m".format(" ".join(string))
    return ret

def blue(*string):
    """ return string as green """
    string = [str(s) for s in string]
    ret = "\033[34m{0}\033[39m".format(" ".join(string))
    return ret

