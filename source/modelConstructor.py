from __future__ import division

try:
    import plotter
    hasPlotting=True
except:
    hasPlotting=False

from modules import *

################################################################################
# This implements the morphing signal model
################################################################################

from ROOT import RooRealVar, TFile, TH1D, RooGaussian
from ROOT import gROOT
gROOT.LoadMacro("../source/classes.h")
from ROOT import HInt,nSigClass


modelConstructorFiles = {}
modelConstructorVars = []
modelConstructorPdfs = []
modelConstructorHists = {}
modelConstructorHasRun = False
def reset():
    global modelConstructorFiles 
    global modelConstructorVars 
    global modelConstructorPdfs 
    global modelConstructorHists 
    global modelConstructorHasRun 
    modelConstructorFiles = {}
    modelConstructorVars = []
    modelConstructorPdfs = []
    modelConstructorHists = {}
    modelConstructorHasRun = False

def nSigBuilder(w):
    nSig = nSigClass("nSig","nSig",w.var("x"),w.obj("lambda"))
    # getattr(w,"import")(nSig)
    return nSig


def morphedIntegral(interference="const",chirality="LL",invert=1,srMin=0,srMax=-1,w=None,channel="ee",pathPrefix="",lumi=140,rebin=1):
    """ This function sets up and returns a RooRealVar 
        representing the normalized integral of the CI
        in the SR
    """
    global modelConstructorFiles, modelConstructorHasRun, modelConstructorPdfs, modelConstructorVars


    signalPath         = "../data/CI_Template_{0}.root".format(chirality)
    # load masses
    signalMasses       = np.arange(12,100,2)
    signalNames        = []
    signalHistNames    = []
    for signalMass in signalMasses:
        signalName     = "{0}_{1}_{2}".format(chirality,interference,signalMass)
        signalHistName = "diff_CI_{0}_{1}_{2}_TeV".format(chirality,interference,signalMass)
        signalNames.append(signalName)
        signalHistNames.append(signalHistName)

    # load hist
    hint = HInt("morph","morph",w.obj("lambdaInv"),invert,float(srMin),float(srMax))

    if signalPath not in modelConstructorFiles.keys():
        f = TFile.Open(signalPath)
        modelConstructorFiles[signalPath]=f

    for name in signalHistNames:
        # print "ADDING HIST",name
        if name not in modelConstructorHists.keys():
            hist = modelConstructorFiles[signalPath].Get(name)
            if not hist: raise BaseException("Did not find hist {0} in file {1}".format(name,signalPath))
            hist.Scale(lumi/80.5)
            [hist.SetBinContent(i,0) for i in range(300)]
            modelConstructorHists[name] = hist
        else:
            hist = modelConstructorHists[name]
        signalMass = int(name.split("_")[4])
        hint.add(hist,signalMass)
    # print ""

    return hint
