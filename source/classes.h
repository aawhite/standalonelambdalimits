#ifndef CCLASS
#define CCLASS

/*
    This file has two classes: one for setting
    limits on Lambda, one for setting limits on
    nSig directly
*/

#include "RooAbsReal.h"
#include "RooRealProxy.h"
#include "RooTrace.h"

class RooRealVar;

/* This class
   Is just a wrapper for retruning lambda
   It keeps the implementation consistent for
   the nSig and Lambda limit implementations
*/
class nSigClass : public RooAbsReal {
public:

    nSigClass(const char *name, const char *title, 
               RooAbsReal& _mll, RooAbsReal& _lambda) :
        RooAbsReal(name,title),
        lambda("lambda","lambda",this,_lambda)
    {
    };

    nSigClass(const nSigClass& other, const char* name=0) :
        RooAbsReal(other,name),
        lambda("lambda",this,other.lambda)
    {
    };

    virtual TObject* clone(const char* newname) const { return new nSigClass(*this,newname); }
    inline virtual ~nSigClass() {  }
    RooRealProxy lambda ;

protected:
    Double_t evaluate() const {
        return lambda;
    };
private:
    ClassDef(nSigClass,1) // Gaussian PDF
};

/* This class
   It takes an integral of the appropriate CI lambda histogram
*/
class HInt : public RooAbsReal {
public:
    HInt(const char *name, const char *title,
         RooAbsReal& _lambda,
         bool invert,
         double srMin,double srMax) :
        _invert(invert),
        _srMin(srMin),
        _srMax(srMax),
        RooAbsReal(name,title),
        lambdaInv("lambdaInv","lambdaInv",this,_lambda)
    {};

    HInt(const HInt& other, const char* name=0) :
        RooAbsReal(other,name),
        lambdaInv("lambdaInv",this,other.lambdaInv)
    {
       int nHists = other._lambdas.size();
       TH1* newHist;
       _invert = other._invert;
       _srMin = other._srMin;
       _srMax = other._srMax;
       _loBin = other._loBin;
       _hiBin = other._hiBin;
       for (int i=0; i<nHists; i++){
           newHist = other._hists[i];
           _hists.push_back(newHist);
           _lambdas.push_back(other._lambdas[i]);
       }
    };

    inline virtual ~HInt() {
        // cout<<">>>>>>>>>>>>>>>>>>>>>>>>\n";
        // cout<<"Calling HInt Destructo\n";
        // cout<<"<<<<<<<<<<<<<<<<<<<<<<<<\n";
        return;
    }

    virtual TObject* clone(const char* newname) const { return new HInt(*this,newname); }

    // #####################################
    // variables
    // #####################################
    bool _initialized;       // keeps track of whether the class has been initialized
    vector<TH1*> _hists;     // vector of histograms
    vector<double> _lambdas; // morphing parameter value
    RooRealProxy lambdaInv ;    // morphing dimension variable
    double _srMin;
    bool _invert;
    double _srMax;
    int _loBin;
    int _hiBin;

    void add(TH1* hist, double paramValue){
        // Add a histogram associated to paramValue.
        // This gets added, coppied, and stored for integrating
        bool status;
        status = this->_addHist(hist,paramValue);
        if (!status){
            cout<<"\n\n==== ERROR ADDING HIST\n";
            cout<<"==== Hist:\n";
            hist->Print();
            cout<<"\n";
        }
        _loBin = hist->FindBin(_srMin);
        _hiBin = hist->FindBin(_srMax);
    }

    double debug(){
        cout<<"\n=========== DEBUG =============\n";
        cout<<"lambdaInv: "<<lambdaInv<<endl;
        cout<<"===============================\n";
        return this->evaluate();
    }


protected:

    bool _addHist(TH1* hist, double paramValue){
        // add a histogram and param
        // check bins for compatibility
        // add histogram
        TH1* copyHist;//needed?
        copyHist = (TH1*)hist->Clone();
        _hists.push_back(copyHist);
        _lambdas.push_back(paramValue);
        return true;
    }

    double _th1Sum(TH1* hist){
        // return integral of th1
        double result = 0;
        for (int i=1; i<hist->GetNbinsX(); i++)
            result+=hist->GetBinContent(i);
        return result;
    }

    Double_t _getLambda() const{
        // do lambda inversion in one place
        // or don't do it
        if (_invert){
            // return (100+lambdaInv);
            return (-lambdaInv);
        }
        else{
            return (lambdaInv);
        }
    }

    Double_t evaluate() const{
        // cout<<":"<<lambdaInv<<":"<<endl;
        int morphIndex = this->_getMorphIndex();
        double lambdaVal = this->_getLambda();
        if (!_hists[morphIndex] || !_hists[morphIndex+1])
           throw std::runtime_error("Expected histogram not found!");

        TH1* h1 = _hists[morphIndex];
        TH1* h2 = _hists[morphIndex+1];

        // cout<<"\n==========================\n";
        // cout<<"== eval\n";
        // cout<<"lambdaVal="<<lambdaVal<<endl;
        // cout<<"morphIndex="<<morphIndex<<endl;
        // cout<<"_loBin="<<_loBin<<endl;
        // cout<<"_hiBin="<<_hiBin<<endl;
        // cout<<"==========================\n";

        double h1Int = h1->Integral(_loBin,_hiBin);
        double h2Int = h2->Integral(_loBin,_hiBin);

        double lambda1 = _lambdas[morphIndex];
        double lambda2 = _lambdas[morphIndex+1];

        double ret = this->_interpolate(lambdaVal,lambda1,h1Int,lambda2,h2Int);
        return ret;
        // return ret/exp(pow(ret,0.87)/11);
    }

    double _interpolate(double x, double x1, double y1, double x2, double y2) const {
        // perform interpolation
        if (x2-x1==0) throw std::runtime_error("Division by zero in Morphed interpolation, x2-x1");
        double m  = (y2-y1)/(x2-x1);
        double y  = y1 + m*(x-x1);
        return y;
    }

    int _getMorphIndex() const{
        // return index of the first _lambdas value which is below lambdaInv->getVal()
        // if lowest _lambdas is higher, returns 0
        // if highest _lambdas is lower, returns nHists-1 (2nd last lambdaInv index)
        double target = this->_getLambda();
        double nHists = _hists.size();
        // edge cases
        if (_lambdas[0]>=target) return 0;
        if (_lambdas[nHists-1]<=target) return nHists-2;
        // search
        for (int i=1; i<nHists; i++){
            // cout<<"check: "<<_lambdas[i]<<" "<<target<<endl;
            if (_lambdas[i]>target) return i-1;
        }
        // should not happen
        cout<<"\n\n";
        cout<<"Finding index for target: "<<target<<endl;
        cout<<"Number of hists:          "<<nHists<<endl;
        cout<<"Printing lambda:          "<<endl;
        lambdaInv.Print();
        cout<<"Has value:                "<<lambdaInv<<endl;
        cout<<"\n\n";
        throw std::runtime_error("Could not find HMorph index in getMorphIndex");
        return 0;
    }

};

ClassImp(HInt);
ClassImp(nSigClass);

#endif

