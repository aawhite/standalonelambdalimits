# Standalone limits comparison (Lambda vs nSig as POI)

This illustrates the different limits I get from two strategies which I expect to give the same limits:

1) Set limits on number of signal events (nSig) where nExp = nSig+nBkg. For one configuration we get for example an upper limit on nSig of 5.40
2) Set limits on "lambda" where a function of lambda determines nSig: f(lambda) = nSig. In this case, nExp = f(lambda)+nBkg. With the same configuration we get a limit on lambda that corresponds to f(lambda)=nSig of 5.71


This will print out the limits

Inside `source/`:
* `classes.h` has RooAbsReal classes for setting limits. This is because the function f(lambda) is implemented in a RooAbsReal. For consistency, the nSig limits go through a RooAbsReal implementation as well. Because a larger Lambda means a smaller contribution to the expected signal yield, the value of lambda is inverted inside the HInt class
* `modelConstructor.py` sets up the classes
* `limits.py` has the functions for setting limits.

# To run
`cd analysis; python2 example.py`
(there are some messy relative paths, so `cd` first.)
Output:
```
==================================================
Limit set with nSig as POI:
nSig < 5.40374801459
--------------------------------------------------
Limit set with Lambda as POI:
nSig < 5.85664239672
==================================================
```

# Illustration: 
This is illustrated by the CLs curves from both these methods. The CLs curves are expected to overlap for identical values of nSig:

## Linear scale:
![In urxvt](data/clsCompLin.png)

## Log scale:
![In urxvt](data/clsComp.png)

